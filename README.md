#api.bazilik.com

#####Auth:
- Required header: "Firebase-Token", "Api-Token"
- Username of credentials equals phone field

#####Roles:
- GUEST -> JWT Api token
- USER -> Firebase token with payload "user"
- ADMIN -> Firebase token with payload "admin"
- GOD -> Firebase token with my phone number

