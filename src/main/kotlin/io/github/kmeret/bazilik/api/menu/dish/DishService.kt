package io.github.kmeret.bazilik.api.menu.dish

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.menu.category.CategoryService
import io.github.kmeret.bazilik.api.storage.file.DomainService
import io.github.kmeret.bazilik.api.storage.image.ImageService
import io.github.kmeret.bazilik.api.storage.image.ImageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class DishService {

    @Autowired
    private lateinit var dishRepo: DishRepository
    @Autowired
    private lateinit var domainService: DomainService
    @Autowired
    private lateinit var imageService: ImageService
    @Autowired
    private lateinit var categoryService: CategoryService

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun getDishById(dishId: Long) = dishRepo.findById(dishId).get()

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllByCategoryId(categoryId: Long) = dishRepo.findAllByCategoryId(categoryId).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllByIds(ids: List<Long>) = dishRepo.findAllById(ids).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun save(dishJson: String, categoryIdJson: String, photo: MultipartFile): Dish {
        val imagePath = imageService.storeImage(photo, ImageType.DISH)
        val categoryId = ObjectMapper().readValue(categoryIdJson, Long::class.java)

        val dish = ObjectMapper().readValue(dishJson, Dish::class.java).apply {
            photoUrl = imagePath
            category = categoryService.findById(categoryId)
        }

        return dishRepo.save(dish)
    }

    fun mapUrl(dish: Dish) = dish.apply {
        if (photoUrl.startsWith("http")) return@apply

        photoUrl = domainService.mapUrl(photoUrl)
        categoryService.mapUrl(category!!)
    }

}
