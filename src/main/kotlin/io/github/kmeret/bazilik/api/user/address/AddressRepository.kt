package io.github.kmeret.bazilik.api.user.address

import org.springframework.data.repository.CrudRepository

interface AddressRepository : CrudRepository<Address, Long> {
    fun findAllByUserId(userId: Long): Iterable<Address>
}