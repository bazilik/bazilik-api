package io.github.kmeret.bazilik.api.user.order

import io.github.kmeret.bazilik.api.api.ApiResponse
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.menu.dish.DishRepository
import io.github.kmeret.bazilik.api.menu.dish.DishService
import io.github.kmeret.bazilik.api.user.address.AddressRepository
import io.github.kmeret.bazilik.api.user.order.cartitem.CartItem
import io.github.kmeret.bazilik.api.user.order.cartitem.CartItemRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class OrderService {

    @Autowired
    private lateinit var addressRepository: AddressRepository
    @Autowired
    private lateinit var cartItemRepository: CartItemRepository
    @Autowired
    private lateinit var orderRepository: OrderRepository
    @Autowired
    private lateinit var dishRepository: DishRepository
    @Autowired
    private lateinit var dishService: DishService

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun save(createOrderDto: CreateOrderDto): ApiResponse {
        return try {
            val address = addressRepository.findById(createOrderDto.addressId).get()
            val order = Order(address = address, comment = createOrderDto.comment)
            orderRepository.save(order)
            createOrderDto.cartItemList.forEach {
                val dish = dishRepository.findById(it.dishId).get()
                val cartItem = CartItem(dish = dish, count = it.count, order = order)
                cartItemRepository.save(cartItem)
            }
            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            ApiResponse(false)
        }
    }

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun findAllDeliveredByUserId(userId: Long): List<DeliveredDishDto> {
        val dishList = ArrayList<DeliveredDishDto>()
        addressRepository.findAllByUserId(userId).forEach {
            it.orderList.filter { it.delivered }.forEach { order ->
                order.cartItemList.forEach cartItemList@{
                    if (it.dish == null) return@cartItemList
                    dishList.add(DeliveredDishDto(dishService.mapUrl(it.dish), it.count, order.createdAt))
                }
            }
        }
        return dishList
    }

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllUndeliveredByShopId(shopId: Long) =
            orderRepository.findAllUndeliveredByShopId(shopId)

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun complete(orderId: Long): ApiResponse {
        return try {
            val order = orderRepository.findById(orderId).get()
            order.delivered = true
            orderRepository.save(order)
            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            ApiResponse(false)
        }
    }

}