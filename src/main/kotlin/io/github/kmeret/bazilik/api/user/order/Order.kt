package io.github.kmeret.bazilik.api.user.order

import io.github.kmeret.bazilik.api.user.address.Address
import io.github.kmeret.bazilik.api.user.order.cartitem.CartItem
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "\"order\"")
data class Order(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        @ManyToOne
        @JoinColumn(name = "address_id")
        val address: Address? = null,
        val comment: String = "",
        val createdAt: Long = Calendar.getInstance().timeInMillis,
        var delivered: Boolean = false,
        @OneToMany(mappedBy = "order")
        var cartItemList: List<CartItem> = emptyList()
)