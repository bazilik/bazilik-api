package io.github.kmeret.bazilik.api.menu.shop

import org.springframework.data.repository.CrudRepository

interface ShopRepository : CrudRepository<Shop, Long> {
    fun findAllByUserId(userId: Long): Iterable<Shop>
}