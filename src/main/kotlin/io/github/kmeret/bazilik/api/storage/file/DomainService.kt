package io.github.kmeret.bazilik.api.storage.file

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

@Service
class DomainService {

    @Autowired
    private lateinit var env: Environment

    fun mapUrl(filePath: String) = "${env.getProperty("app.domain").toString()}/$filePath"

}