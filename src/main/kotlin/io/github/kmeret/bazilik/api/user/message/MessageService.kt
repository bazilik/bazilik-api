package io.github.kmeret.bazilik.api.user.message

import io.github.kmeret.bazilik.api.api.ApiResponse
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.auth.user.UserService
import io.github.kmeret.bazilik.api.menu.dish.DishService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class MessageService {

    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var dishService: DishService
    @Autowired
    private lateinit var messageRepository: MessageRepository

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun sendDish(dishMessageDto: DishMessageDto): ApiResponse {
        return try {
            val userFrom = userService.getUserById(dishMessageDto.userId)
            val userTo = userService.getUserById(dishMessageDto.friendId)
            val dish = dishService.getDishById(dishMessageDto.dishId)
            messageRepository.save(Message(userFrom = userFrom,
                    userTo = userTo,
                    dish = dish,
                    comment = dishMessageDto.comment))
            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            ApiResponse(false)
        }
    }

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun getUnviewedMessageListByUserId(userId: Long) =
            messageRepository.findAllByUserToId(userId).filterNot { it.viewed }.map(::mapUrl)

    fun mapUrl(message: Message) = message.apply {
        userFrom = userService.mapUrl(userFrom!!)
        userTo = userService.mapUrl(userTo!!)
        dish = dishService.mapUrl(dish!!)
    }

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun viewMessageList(viewedMessageListDto: ViewedMessageListDto): ApiResponse {
        return try {
            viewedMessageListDto.idList.forEach {
                val message = messageRepository.findById(it).get()
                message.viewed = true
                messageRepository.save(message)
            }
            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            ApiResponse(false)
        }
    }

}
