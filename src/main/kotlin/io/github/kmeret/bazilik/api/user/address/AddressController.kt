package io.github.kmeret.bazilik.api.user.address

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("address")
class AddressController {

    @Autowired
    private lateinit var addressService: AddressService

    @GetMapping("all")
    fun getAllByUserId(@RequestParam("userId") userId: Long) =
            addressService.findAllByUserId(userId)

    @PostMapping("create")
    fun save(@RequestBody addressDto: AddressDto) =
            addressService.save(addressDto)

    @DeleteMapping("delete")
    fun deleteById(@RequestParam("addressId") addressId: Long) =
            addressService.deleteById(addressId)

}