package io.github.kmeret.bazilik.api.user.order.cartitem

import org.springframework.data.repository.CrudRepository

interface CartItemRepository : CrudRepository<CartItem, Long>