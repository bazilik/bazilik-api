package io.github.kmeret.bazilik.api.user.feedback

import io.github.kmeret.bazilik.api.api.ApiResponse
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.auth.user.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class FeedbackService {

    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var feedbackRepository: FeedbackRepository

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun save(feedbackDto: FeedbackDto): ApiResponse {
        return try {
            val user = userRepository.findById(feedbackDto.userId).get()
            feedbackRepository.save(Feedback(user = user, text = feedbackDto.text))
            ApiResponse(true)
        } catch (ex: Exception) {
            println(ex.message)
            ApiResponse(false)
        }
    }

}