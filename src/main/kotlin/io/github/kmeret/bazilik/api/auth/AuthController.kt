package io.github.kmeret.bazilik.api.auth

import io.github.kmeret.bazilik.api.auth.api.ApiTokenService
import io.github.kmeret.bazilik.api.auth.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("auth")
class AuthController {

    data class ApiToken(val token: String)

    @Autowired
    private lateinit var apiTokenService: ApiTokenService

    @Autowired
    private lateinit var userService: UserService

    @GetMapping("api")
    fun getApiToken() = ApiToken(apiTokenService.getToken())

    @GetMapping("check")
    fun checkUserIsExists(@RequestParam("phone") phone: String) =
            userService.isUserExists(phone)

    @PostMapping("register")
    @ResponseBody
    fun create(@RequestPart("user") userJson: String,
               @RequestPart("avatar") photo: MultipartFile) =
            userService.save(userJson, photo)

}