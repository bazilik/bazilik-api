package io.github.kmeret.bazilik.api.auth.user

import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Long> {
    fun findByPhone(phone: String): User?
}