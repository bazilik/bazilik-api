package io.github.kmeret.bazilik.api.user.message

import io.github.kmeret.bazilik.api.auth.user.User
import io.github.kmeret.bazilik.api.menu.dish.Dish
import javax.persistence.*

@Entity
data class Message(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val comment: String = "",
        val sentAt: Long = System.currentTimeMillis(),
        var viewed: Boolean = false,
        @ManyToOne
        @JoinColumn(name = "user_from_id")
        var userFrom: User? = null,
        @ManyToOne
        @JoinColumn(name = "user_to_id")
        var userTo: User? = null,
        @ManyToOne
        @JoinColumn(name = "dish_id")
        var dish: Dish? = null
)