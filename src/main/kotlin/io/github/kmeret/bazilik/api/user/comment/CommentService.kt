package io.github.kmeret.bazilik.api.user.comment

import io.github.kmeret.bazilik.api.api.ApiResponse
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.auth.user.UserService
import io.github.kmeret.bazilik.api.menu.dish.DishRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class CommentService {

    @Autowired
    private lateinit var commentRepo: CommentRepository
    @Autowired
    private lateinit var dishRepo: DishRepository

    @Autowired
    private lateinit var userService: UserService

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun save(commentDto: CommentDto): Comment {
        val user = userService.getUserById(commentDto.userId)
        val dish = dishRepo.findById(commentDto.dishId).get()
        dish.commentCount += 1
        dishRepo.save(dish)
        return commentRepo.save(Comment(text = commentDto.text, user = user, dish = dish))
    }

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun findAllByDishId(dishId: Long) =
            commentRepo.findAllByDishId(dishId).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllByShopId(shopId: Long) =
            commentRepo.findAllByShopId(shopId).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun deleteComment(commentId: Long): ApiResponse {
        return try {
            val dish = commentRepo.findById(commentId).get().dish!!
            dish.commentCount -= 1
            dishRepo.save(dish)
            commentRepo.deleteById(commentId)
            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            ApiResponse(false)
        }
    }

    fun mapUrl(comment: Comment) = comment.apply {
        user = userService.mapUrl(user!!)
    }

}