package io.github.kmeret.bazilik.api.auth.api

import io.github.kmeret.bazilik.api.auth.role.RoleRepository
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.impl.DefaultClaims
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import java.util.*

@Service
class ApiTokenService {

    @Autowired
    private lateinit var env: Environment

    @Autowired
    private lateinit var roleRepository: RoleRepository

    fun getToken(): String {

        val expiredAt = Calendar.getInstance().apply {
            add(Calendar.WEEK_OF_MONTH, 1)
        }

        val tokenData = HashMap<String, Any>().apply {
            put("token_expiration_date", expiredAt.time.time)
        }

        return Jwts.builder().apply {
            setExpiration(expiredAt.time)
            setClaims(tokenData)
        }.signWith(SignatureAlgorithm.HS512, getEncodedSecret()).compact()
    }

    fun verifyToken(token: String): Boolean {

        val claims = Jwts.parser()
                .setSigningKey(getEncodedSecret())
                .parse(token).body as DefaultClaims

        val expiredAt = claims.get("token_expiration_date", Long::class.javaObjectType)
                ?: throw Exception("Invalid token")

        return (Date(expiredAt).after(Date()))
    }

    fun getRoleByName(name: String) = roleRepository.findByName(name)

    private fun getEncodedSecret() =
            Base64.getEncoder().encode(env.getProperty("app.api-secret")?.toByteArray())

}