package io.github.kmeret.bazilik.api.user.comment

data class CommentDto(
        val text: String,
        val userId: Long,
        val dishId: Long
)