package io.github.kmeret.bazilik.api.user.contact

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("contact")
class ContactController {

    @Autowired
    private lateinit var contactService: ContactService

    @PostMapping("import")
    fun importContactsList(@RequestBody contactsDto: ContactsDto) =
            contactService.importContacts(contactsDto)

    @GetMapping("friends")
    fun getFriendList(@RequestParam("userId") userId: Long) =
            contactService.getFriendListByUserId(userId)

}