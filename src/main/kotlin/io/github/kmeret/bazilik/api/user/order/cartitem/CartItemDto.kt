package io.github.kmeret.bazilik.api.user.order.cartitem

data class CartItemDto(val dishId: Long, val count: Int)