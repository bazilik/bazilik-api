package io.github.kmeret.bazilik.api.auth

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class AuthHolder(roleList: Collection<GrantedAuthority>)
    : UsernamePasswordAuthenticationToken(null, null, roleList)