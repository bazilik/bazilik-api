package io.github.kmeret.bazilik.api.user.feedback

import io.github.kmeret.bazilik.api.auth.user.User
import javax.persistence.*

@Entity
data class Feedback(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val text: String = "",
        @ManyToOne
        @JoinColumn(name = "user_id")
        val user: User? = null
)