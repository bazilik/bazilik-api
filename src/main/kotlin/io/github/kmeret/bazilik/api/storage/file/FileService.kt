package io.github.kmeret.bazilik.api.storage.file

import io.github.kmeret.bazilik.api.auth.role.Roles
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

@Service
class FileService {

    private val rootLocation = Paths.get("files")

    fun get(filename: String): Resource {
        try {
            val file = rootLocation.resolve(filename)
            val resource = UrlResource(file.toUri())
            if (resource.exists() || resource.isReadable) {
                return resource
            } else throw Exception("Could not read file: $filename")
        } catch (ex: Exception) {
            throw Exception("Could not read file: $filename", ex)
        }
    }

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun store(file: MultipartFile, path: String) {
        val filename = file.originalFilename ?: return
        try {
            if (file.isEmpty) throw Exception("Failed to store empty file $filename")
            if (filename.contains("..")) throw Exception("Cannot store file with relative path " +
                    "outside current directory $filename")

            val filePath = rootLocation.resolve(path)

            Files.createDirectories(filePath)
            Files.copy(file.inputStream, filePath, StandardCopyOption.REPLACE_EXISTING)

        } catch (ex: Exception) {
            throw Exception("Failed to store file $filename", ex)
        }
    }

}
