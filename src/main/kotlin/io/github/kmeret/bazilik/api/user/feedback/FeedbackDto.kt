package io.github.kmeret.bazilik.api.user.feedback

data class FeedbackDto(val userId: Long, val text: String)