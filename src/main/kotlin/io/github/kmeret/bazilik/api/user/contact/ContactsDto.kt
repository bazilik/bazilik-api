package io.github.kmeret.bazilik.api.user.contact

data class ContactsDto(val userId: Long, val phoneList: List<String>)