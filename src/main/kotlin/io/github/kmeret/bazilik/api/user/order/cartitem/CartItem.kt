package io.github.kmeret.bazilik.api.user.order.cartitem

import com.fasterxml.jackson.annotation.JsonIgnore
import io.github.kmeret.bazilik.api.menu.dish.Dish
import io.github.kmeret.bazilik.api.user.order.Order
import javax.persistence.*

@Entity
data class CartItem(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        @ManyToOne
        @JoinColumn(name = "dish_id")
        val dish: Dish? = null,
        val count: Int = 0,
        @ManyToOne
        @JoinColumn(name = "order_id")
        @JsonIgnore
        val order: Order? = null
)