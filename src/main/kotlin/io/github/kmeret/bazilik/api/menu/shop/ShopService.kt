package io.github.kmeret.bazilik.api.menu.shop

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.storage.file.DomainService
import io.github.kmeret.bazilik.api.storage.image.ImageService
import io.github.kmeret.bazilik.api.storage.image.ImageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class ShopService {

    @Autowired
    private lateinit var shopRepo: ShopRepository
    @Autowired
    private lateinit var domainService: DomainService
    @Autowired
    private lateinit var imageService: ImageService

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun findAll() = shopRepo.findAll().map(::mapUrl)

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun findById(id: Long) = mapUrl(shopRepo.findById(id).get())

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllByUserId(userId: Long) = shopRepo.findAllByUserId(userId).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun save(shopJson: String, photo: MultipartFile): Shop {

        val shop = ObjectMapper().readValue(shopJson, Shop::class.java).apply {
            logoUrl = imageService.storeImage(photo, ImageType.LOGO)
        }

        return shopRepo.save(shop)
    }

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun mapUrl(shop: Shop) = shop.apply {
        if (logoUrl.startsWith("http")) return@apply

        logoUrl = domainService.mapUrl(logoUrl)
    }


}
