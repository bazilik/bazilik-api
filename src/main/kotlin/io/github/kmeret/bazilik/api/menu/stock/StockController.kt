package io.github.kmeret.bazilik.api.menu.stock

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("stock")
class StockController {

    @Autowired
    private lateinit var stockService: StockService

    @GetMapping("all")
    fun getAll() = stockService.findAll()

    @GetMapping
    fun getByShopId(@RequestParam("shopId") shopId: Long) =
            stockService.findAllByShopId(shopId)

    @PostMapping
    @ResponseBody
    fun create(@RequestPart("stock") stockJson: String,
               @RequestPart("shopId") shopIdJson: String,
               @RequestPart("photo") photo: MultipartFile) =
            stockService.save(stockJson, shopIdJson, photo)

}
