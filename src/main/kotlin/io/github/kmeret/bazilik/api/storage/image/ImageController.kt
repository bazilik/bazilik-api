package io.github.kmeret.bazilik.api.storage.image

import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.HandlerMapping
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping(ImageController.TAG)
class ImageController(private val imageService: ImageService) {

    companion object {
        const val TAG = "img"
    }

    @GetMapping("**")
    @ResponseBody
    fun serve(request: HttpServletRequest): ResponseEntity<Resource> {

        val filename = request
                .getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)
                .toString()
                .removePrefix("/")

        val file = imageService.getImage(filename)

        return ResponseEntity.ok()
                .header(HttpHeaders.CACHE_CONTROL, "max-age=604800")
                .header(HttpHeaders.CONTENT_TYPE, "image/${file.file.extension}")
                .body(file)
    }

}