package io.github.kmeret.bazilik.api.user.message

data class DishMessageDto(
        val userId: Long,
        val friendId: Long,
        val dishId: Long,
        val comment: String
)