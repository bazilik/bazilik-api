package io.github.kmeret.bazilik.api.user.message

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("message")
class MessageController {

    @Autowired
    private lateinit var messageService: MessageService

    @PostMapping("send/dish")
    fun sendDish(@RequestBody dishMessageDto: DishMessageDto) =
            messageService.sendDish(dishMessageDto)

    @GetMapping("unviewed")
    fun getUnviewedMessageList(@RequestParam("userId") userId: Long) =
            messageService.getUnviewedMessageListByUserId(userId)

    @PostMapping("view")
    fun viewMessageList(@RequestBody viewedMessageListDto: ViewedMessageListDto) =
            messageService.viewMessageList(viewedMessageListDto)

}