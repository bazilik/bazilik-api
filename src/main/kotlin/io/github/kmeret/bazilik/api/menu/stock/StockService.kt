package io.github.kmeret.bazilik.api.menu.stock

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.menu.shop.ShopService
import io.github.kmeret.bazilik.api.storage.file.DomainService
import io.github.kmeret.bazilik.api.storage.image.ImageService
import io.github.kmeret.bazilik.api.storage.image.ImageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class StockService {

    @Autowired
    private lateinit var stockRepo: StockRepository
    @Autowired
    private lateinit var domainService: DomainService
    @Autowired
    private lateinit var imageService: ImageService
    @Autowired
    private lateinit var shopService: ShopService

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAll() = stockRepo.findAll().map(::mapUrl)

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllByShopId(shopId: Long) = stockRepo.findAllByShopId(shopId).map(::mapUrl)

    fun save(stockJson: String, shopIdJson: String, photo: MultipartFile): Stock {
        val imagePath = imageService.storeImage(photo, ImageType.STOCK)
        val shopId = ObjectMapper().readValue(shopIdJson, Long::class.java)

        val stock = ObjectMapper().readValue(stockJson, Stock::class.java).apply {
            photoUrl = imagePath
            shop = shopService.findById(shopId)
        }

        return stockRepo.save(stock)
    }

    fun mapUrl(stock: Stock) = stock.apply {
        if (photoUrl.startsWith("http")) return@apply

        photoUrl = domainService.mapUrl(photoUrl)
    }

}
