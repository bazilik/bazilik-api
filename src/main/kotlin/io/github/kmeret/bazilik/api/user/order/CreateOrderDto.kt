package io.github.kmeret.bazilik.api.user.order

import io.github.kmeret.bazilik.api.user.order.cartitem.CartItemDto

data class CreateOrderDto(val addressId: Long,
                          val comment: String,
                          val cartItemList: List<CartItemDto>)