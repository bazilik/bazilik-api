package io.github.kmeret.bazilik.api.api

data class ApiResponse(val isSuccess: Boolean)