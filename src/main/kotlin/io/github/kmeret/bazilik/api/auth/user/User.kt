package io.github.kmeret.bazilik.api.auth.user

import com.fasterxml.jackson.annotation.JsonIgnore
import io.github.kmeret.bazilik.api.auth.role.Role
import io.github.kmeret.bazilik.api.menu.shop.Shop
import io.github.kmeret.bazilik.api.user.address.Address
import io.github.kmeret.bazilik.api.user.feedback.Feedback
import javax.persistence.*

@Entity
@Table(name = "\"user\"")
data class User(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val fullName: String = "",
        val phone: String = "",
        var avatarUrl: String = "",
        val isNotificationEnabled: Boolean = false,
        @ManyToMany(fetch = FetchType.EAGER, cascade = [(CascadeType.ALL)])
        @JsonIgnore
        var roleList: MutableList<Role> = arrayListOf(),
        @OneToMany(mappedBy = "user")
        @JsonIgnore
        var addressList: MutableList<Address> = arrayListOf(),
        @OneToMany(mappedBy = "user")
        @JsonIgnore
        var feedbackList: MutableList<Feedback> = arrayListOf(),
        @ManyToMany(fetch = FetchType.EAGER, cascade = [(CascadeType.ALL)])
        @JsonIgnore
        var friendList: MutableList<User> = arrayListOf(),
        @OneToMany(mappedBy = "user")
        @JsonIgnore
        var shopList: MutableList<Shop> = arrayListOf()
)