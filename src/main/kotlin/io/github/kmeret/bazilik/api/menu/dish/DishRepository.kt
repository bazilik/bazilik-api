package io.github.kmeret.bazilik.api.menu.dish

import org.springframework.data.repository.CrudRepository

interface DishRepository : CrudRepository<Dish, Long> {
    fun findAllByCategoryId(categoryId: Long): Iterable<Dish>
}