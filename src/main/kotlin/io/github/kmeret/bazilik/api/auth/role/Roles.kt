package io.github.kmeret.bazilik.api.auth.role

object Roles {
    const val ROLE_GOD = "ROLE_GOD"
    const val ROLE_ADMIN = "ROLE_ADMIN"
    const val ROLE_USER = "ROLE_USER"
    const val ROLE_GUEST = "ROLE_GUEST"
    val ROLE_LIST = listOf(ROLE_GOD, ROLE_ADMIN, ROLE_USER, ROLE_GUEST)
    //    @Secured(value = [(Roles.ROLE_GOD)])
    //    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    //    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    //    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
}