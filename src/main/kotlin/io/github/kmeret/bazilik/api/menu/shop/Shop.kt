package io.github.kmeret.bazilik.api.menu.shop

import com.fasterxml.jackson.annotation.JsonIgnore
import io.github.kmeret.bazilik.api.auth.user.User
import io.github.kmeret.bazilik.api.menu.category.Category
import io.github.kmeret.bazilik.api.menu.stock.Stock
import javax.persistence.*

@Entity
data class Shop(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val name: String = "",
        var logoUrl: String = "",
        @ManyToOne
        @JoinColumn(name = "user_id")
        var user: User? = null,
        @OneToMany(mappedBy = "shop")
        @JsonIgnore
        var categoryList: List<Category> = emptyList(),
        @OneToMany(mappedBy = "shop")
        @JsonIgnore
        var stockList: List<Stock> = emptyList()
)