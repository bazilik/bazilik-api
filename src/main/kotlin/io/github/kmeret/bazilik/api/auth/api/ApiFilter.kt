package io.github.kmeret.bazilik.api.auth.api

import io.github.kmeret.bazilik.api.auth.AuthHolder
import io.github.kmeret.bazilik.api.auth.role.Roles
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class ApiFilter(private val apiTokenService: ApiTokenService) : GenericFilterBean() {

    companion object {
        const val TOKEN_HEADER = "Api-Token"
    }

    override fun doFilter(request: ServletRequest,
                          response: ServletResponse,
                          chain: FilterChain) {

        val token = (request as HttpServletRequest).getHeader(TOKEN_HEADER)

        if (token.isNullOrEmpty() || !apiTokenService.verifyToken(token)) {
            chain.doFilter(request, response)
            return
        }

        val guestRole = apiTokenService.getRoleByName(Roles.ROLE_GUEST)
        if (guestRole == null) {
            println("Guest role not found!")
            chain.doFilter(request, response)
            return
        }

        val auth = SecurityContextHolder.getContext().authentication
        if (auth == null) {
            SecurityContextHolder.getContext().authentication = AuthHolder(listOf(guestRole))
            chain.doFilter(request, response)
            return
        }

        val roleList = ArrayList<GrantedAuthority>().apply {
            addAll(auth.authorities)
            add(guestRole)
        }

        SecurityContextHolder.getContext().authentication = AuthHolder(roleList)
        chain.doFilter(request, response)
    }

}