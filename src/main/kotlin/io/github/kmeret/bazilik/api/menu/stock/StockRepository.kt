package io.github.kmeret.bazilik.api.menu.stock

import org.springframework.data.repository.CrudRepository

interface StockRepository : CrudRepository<Stock, Long> {
    fun findAllByShopId(shopId: Long): Iterable<Stock>
}