package io.github.kmeret.bazilik.api.user.order

import io.github.kmeret.bazilik.api.menu.dish.Dish

data class DeliveredDishDto(val dish: Dish, val count: Int, val createdAt: Long)