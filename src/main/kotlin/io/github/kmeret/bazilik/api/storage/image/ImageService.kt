package io.github.kmeret.bazilik.api.storage.image

import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.storage.file.FileService
import io.github.kmeret.bazilik.api.storage.file.UIDService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class ImageService {

    @Autowired
    private lateinit var uidService: UIDService
    @Autowired
    private lateinit var fileService: FileService

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun storeImage(file: MultipartFile, type: ImageType): String {
        val path = getImagePath(type, getImageExt(file))
        fileService.store(file, path)
        return path
    }

    fun getImage(filename: String) = fileService.get(filename)

    private fun getImagePath(type: ImageType, ext: String) = "${ImageController.TAG}/${type.name[0].toLowerCase()}/${uidService.get(2)}/${uidService.get(2)}/${uidService.get(11)}.$ext"

    private fun getImageExt(file: MultipartFile): String {
        val contentType = file.contentType ?: throw NullPointerException("Ext")
        return contentType.split('/')[1]
    }

}
