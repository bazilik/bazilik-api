package io.github.kmeret.bazilik.api.menu.category

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("category")
class CategoryController {

    @Autowired
    private lateinit var categoryService: CategoryService

    @GetMapping
    fun getByShopId(@RequestParam("shopId") shopId: Long) =
            categoryService.findAllByShopId(shopId)

    @PostMapping
    @ResponseBody
    fun create(@RequestPart("category") categoryJson: String,
               @RequestPart("shopId") shopIdJson: String,
               @RequestPart("photo") photo: MultipartFile) =
            categoryService.save(categoryJson, shopIdJson, photo)

}
