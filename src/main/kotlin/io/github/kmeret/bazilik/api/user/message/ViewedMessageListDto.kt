package io.github.kmeret.bazilik.api.user.message

data class ViewedMessageListDto(val idList: List<Long>)