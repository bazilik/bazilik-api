package io.github.kmeret.bazilik.api.user.address

data class AddressDto(val text: String, val userId: Long)