package io.github.kmeret.bazilik.api.user.feedback

import org.springframework.data.repository.CrudRepository

interface FeedbackRepository : CrudRepository<Feedback, Long>