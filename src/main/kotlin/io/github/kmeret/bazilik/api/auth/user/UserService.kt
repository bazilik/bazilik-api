package io.github.kmeret.bazilik.api.auth.user

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.kmeret.bazilik.api.auth.role.RoleRepository
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.storage.file.DomainService
import io.github.kmeret.bazilik.api.storage.image.ImageService
import io.github.kmeret.bazilik.api.storage.image.ImageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class UserService {

    data class UserExists(val isExists: Boolean, val user: User?)

    @Autowired
    private lateinit var userRepo: UserRepository
    @Autowired
    private lateinit var roleRepo: RoleRepository
    @Autowired
    private lateinit var domainService: DomainService
    @Autowired
    private lateinit var imageService: ImageService

    fun grantUserRole(phone: String) = grantRole(phone, Roles.ROLE_USER)

    @Secured(value = [(Roles.ROLE_GUEST)])
    fun isUserExists(phone: String): UserExists {
        var user = userRepo.findByPhone(phone)
        val isExists = user != null
        if (isExists) user = mapUrl(user!!)
        return UserExists(isExists, user)
    }

    @Secured(value = [(Roles.ROLE_GUEST)])
    fun save(userJson: String, photo: MultipartFile): User {
        val user = ObjectMapper().readValue(userJson, User::class.java).apply {
            avatarUrl = imageService.storeImage(photo, ImageType.AVATAR)
        }
        return mapUrl(userRepo.save(user))
    }

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun getUserById(userId: Long) = userRepo.findById(userId).get()

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun getFriendListByUserId(userId: Long) = userRepo.findById(userId).get().friendList.map(::mapUrl)

    fun mapUrl(user: User) = user.apply {
        if (avatarUrl.startsWith("http")) return@apply

        avatarUrl = domainService.mapUrl(avatarUrl)
    }

    private fun grantRole(phone: String, roleName: String): User? {
        val user = userRepo.findByPhone(phone) ?: return null
        val role = roleRepo.findByName(roleName) ?: throw NullPointerException("Role")
        if (user.roleList.contains(role)) return user
        user.roleList.add(role)
        return mapUrl(userRepo.save(user))
    }

}
