package io.github.kmeret.bazilik.api.auth.firebase

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import io.github.kmeret.bazilik.api.auth.AuthHolder
import io.github.kmeret.bazilik.api.auth.user.UserService
import org.springframework.core.io.ClassPathResource
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class FirebaseFilter(private val userService: UserService) : GenericFilterBean() {

    companion object {
        const val TOKEN_HEADER = "Firebase-Token"
    }

    private val firebaseAuth: FirebaseAuth by lazy {
        val stream = ClassPathResource("firebase.json").inputStream
        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(stream))
                .setDatabaseUrl("https://bazilik-2f202.firebaseio.com")
                .build()
        FirebaseApp.initializeApp(options)
        FirebaseAuth.getInstance()
    }

    override fun doFilter(request: ServletRequest,
                          response: ServletResponse,
                          chain: FilterChain) {

        val token = (request as HttpServletRequest).getHeader(TOKEN_HEADER)

        if (token.isNullOrEmpty()) {
            chain.doFilter(request, response)
            return
        }

        try {
            val validToken = firebaseAuth.verifyIdTokenAsync(token).get()
            val phoneNumber = firebaseAuth.getUserAsync(validToken.uid).get().phoneNumber

            val user = userService.grantUserRole(phoneNumber)
            if (user == null) {
                println("User not found!")
                chain.doFilter(request, response)
                return
            }

            val auth = SecurityContextHolder.getContext().authentication
            if (auth == null) {
                SecurityContextHolder.getContext().authentication = AuthHolder(user.roleList)
                chain.doFilter(request, response)
                return
            }

            val roleList = ArrayList<GrantedAuthority>().apply {
                addAll(auth.authorities)
                addAll(user.roleList)
            }

            SecurityContextHolder.getContext().authentication = AuthHolder(roleList)
            chain.doFilter(request, response)

        } catch (ex: Exception) {
            chain.doFilter(request, response)
        }
    }

}