package io.github.kmeret.bazilik.api.menu.category

import com.fasterxml.jackson.annotation.JsonIgnore
import io.github.kmeret.bazilik.api.menu.dish.Dish
import io.github.kmeret.bazilik.api.menu.shop.Shop
import javax.persistence.*

@Entity
data class Category(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val name: String = "",
        var photoUrl: String = "",
        @ManyToOne
        @JoinColumn(name = "shop_id")
        var shop: Shop? = null,
        @OneToMany(mappedBy = "category")
        @JsonIgnore
        var dishList: List<Dish> = emptyList()
)