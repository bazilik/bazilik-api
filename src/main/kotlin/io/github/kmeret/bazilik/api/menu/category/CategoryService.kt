package io.github.kmeret.bazilik.api.menu.category

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.menu.shop.ShopService
import io.github.kmeret.bazilik.api.storage.file.DomainService
import io.github.kmeret.bazilik.api.storage.image.ImageService
import io.github.kmeret.bazilik.api.storage.image.ImageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class CategoryService {

    @Autowired
    private lateinit var categoryRepo: CategoryRepository
    @Autowired
    private lateinit var domainService: DomainService
    @Autowired
    private lateinit var shopService: ShopService
    @Autowired
    private lateinit var imageService: ImageService

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findById(id: Long) = categoryRepo.findById(id).get()

    @Secured(value = [(Roles.ROLE_GUEST), (Roles.ROLE_USER), (Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun findAllByShopId(shopId: Long) = categoryRepo.findAllByShopId(shopId).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_ADMIN), (Roles.ROLE_GOD)])
    fun save(categoryJson: String, shopIdJson: String, photo: MultipartFile): Category {

        val imagePath = imageService.storeImage(photo, ImageType.CATEGORY)
        val shopId = ObjectMapper().readValue(shopIdJson, Long::class.java)

        val category = ObjectMapper().readValue(categoryJson, Category::class.java).apply {
            photoUrl = imagePath
            shop = shopService.findById(shopId)
        }

        return categoryRepo.save(category)
    }

    fun mapUrl(category: Category) = category.apply {
        if (photoUrl.startsWith("http")) return@apply

        photoUrl = domainService.mapUrl(photoUrl)
        shopService.mapUrl(shop!!)
    }

}
