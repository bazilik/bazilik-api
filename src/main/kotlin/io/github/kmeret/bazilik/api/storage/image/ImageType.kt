package io.github.kmeret.bazilik.api.storage.image

enum class ImageType { AVATAR, LOGO, CATEGORY, DISH, STOCK }