package io.github.kmeret.bazilik.api.user.comment

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface CommentRepository : JpaRepository<Comment, Long> {
    fun findAllByDishId(dishId: Long): Iterable<Comment>

    @Query("select distinct com " +
            "from Comment com, Dish d, Category c, Shop s " +
            "where com.dish.id = d.id " +
            "and d.category.id = c.id " +
            "and c.shop.id = :shopId " +
            "order by com.createdAt desc")
    fun findAllByShopId(shopId: Long): Iterable<Comment>
}