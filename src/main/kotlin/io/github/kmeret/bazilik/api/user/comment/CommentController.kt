package io.github.kmeret.bazilik.api.user.comment

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("comment")
class CommentController {

    @Autowired
    private lateinit var commentService: CommentService

    @PostMapping("add")
    fun add(@RequestBody commentDto: CommentDto) =
            commentService.save(commentDto)

    @GetMapping("list")
    fun getAllByDishId(@RequestParam dishId: Long) =
            commentService.findAllByDishId(dishId)

    @GetMapping("shop")
    fun getAllByShopId(@RequestParam shopId: Long) =
            commentService.findAllByShopId(shopId)

    @PostMapping("delete")
    fun deleteComment(@RequestParam commentId: Long) =
            commentService.deleteComment(commentId)

}