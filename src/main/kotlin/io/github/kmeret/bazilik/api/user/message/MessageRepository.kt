package io.github.kmeret.bazilik.api.user.message

import org.springframework.data.repository.CrudRepository

interface MessageRepository : CrudRepository<Message, Long> {
    fun findAllByUserToId(userId: Long): Iterable<Message>
}