package io.github.kmeret.bazilik.api.user.comment

import io.github.kmeret.bazilik.api.auth.user.User
import io.github.kmeret.bazilik.api.menu.dish.Dish
import javax.persistence.*

@Entity
data class Comment(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val text: String = "",
        val createdAt: Long = System.currentTimeMillis(),
        @ManyToOne
        @JoinColumn(name = "user_id")
        var user: User? = null,
        @ManyToOne
        @JoinColumn(name = "dish_id")
        val dish: Dish? = null
)