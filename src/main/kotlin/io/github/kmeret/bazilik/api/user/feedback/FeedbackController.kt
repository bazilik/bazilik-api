package io.github.kmeret.bazilik.api.user.feedback

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("feedback")
class FeedbackController {

    @Autowired
    private lateinit var feedbackService: FeedbackService

    @PostMapping("create")
    fun save(@RequestBody feedbackDto: FeedbackDto) =
            feedbackService.save(feedbackDto)
}