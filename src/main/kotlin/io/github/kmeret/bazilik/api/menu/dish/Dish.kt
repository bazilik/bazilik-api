package io.github.kmeret.bazilik.api.menu.dish

import com.fasterxml.jackson.annotation.JsonIgnore
import io.github.kmeret.bazilik.api.menu.category.Category
import io.github.kmeret.bazilik.api.user.comment.Comment
import io.github.kmeret.bazilik.api.user.order.cartitem.CartItem
import javax.persistence.*

@Entity
data class Dish(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val name: String = "",
        val description: String = "",
        val price: Float? = 0.0f,
        var photoUrl: String = "",
        @ManyToOne
        @JoinColumn(name = "category_id")
        var category: Category? = null,
        @OneToMany(mappedBy = "dish")
        @JsonIgnore
        var cartItemList: List<CartItem> = emptyList(),
        @OneToMany(mappedBy = "dish")
        @JsonIgnore
        val commentList: List<Comment> = emptyList(),
        var commentCount: Int = 0
)