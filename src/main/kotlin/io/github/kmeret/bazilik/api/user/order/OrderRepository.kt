package io.github.kmeret.bazilik.api.user.order

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface OrderRepository : JpaRepository<Order, Long> {
    @Query("select distinct ord " +
            "from Order ord, CartItem ci, Dish d, Category c, Shop s " +
            "where ci.order.id = ord.id " +
            "and ci.dish.id = d.id " +
            "and d.category.id = c.id " +
            "and c.shop.id = s.id " +
            "and s.id = :shopId " +
            "and ord.delivered = false")
    fun findAllUndeliveredByShopId(shopId: Long): Iterable<Order>
}