package io.github.kmeret.bazilik.api.menu.dish

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("dish")
class DishController {

    @Autowired
    private lateinit var dishService: DishService

    @GetMapping
    fun getByCategoryId(@RequestParam("categoryId") categoryId: Long) =
            dishService.findAllByCategoryId(categoryId)

    @GetMapping("find")
    fun getByIds(@RequestParam("id") ids: List<Long>) =
            dishService.findAllByIds(ids)

    @PostMapping
    @ResponseBody
    fun create(@RequestPart("dish") dishJson: String,
               @RequestPart("categoryId") categoryIdJson: String,
               @RequestPart("photo") photo: MultipartFile) =
            dishService.save(dishJson, categoryIdJson, photo)

}
