package io.github.kmeret.bazilik.api.menu.shop

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("shop")
class ShopController {

    @Autowired
    private lateinit var shopService: ShopService

    @GetMapping("all")
    fun getAll() =
            shopService.findAll()

    @GetMapping
    fun getById(@RequestParam("id") id: Long) =
            shopService.findById(id)

    @GetMapping("owner")
    fun getByUserId(@RequestParam("userId") userId: Long) =
            shopService.findAllByUserId(userId)

    @PostMapping
    @ResponseBody
    fun create(@RequestPart("shop") shopJson: String,
               @RequestPart("logo") photo: MultipartFile) =
            shopService.save(shopJson, photo)

}