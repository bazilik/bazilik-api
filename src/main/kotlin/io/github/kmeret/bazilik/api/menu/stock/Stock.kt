package io.github.kmeret.bazilik.api.menu.stock

import io.github.kmeret.bazilik.api.menu.shop.Shop
import javax.persistence.*

@Entity
data class Stock(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val description: String = "",
        val expiredAt: Long = 0,
        var photoUrl: String = "",
        @ManyToOne
        @JoinColumn(name = "shop_id")
        var shop: Shop? = null
)