package io.github.kmeret.bazilik.api.user.address

import com.fasterxml.jackson.annotation.JsonIgnore
import io.github.kmeret.bazilik.api.auth.user.User
import io.github.kmeret.bazilik.api.user.order.Order
import javax.persistence.*

@Entity
data class Address(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val text: String = "",
        @ManyToOne
        @JoinColumn(name = "user_id")
        var user: User? = null,
        @OneToMany(mappedBy = "address")
        @JsonIgnore
        var orderList: List<Order> = emptyList()
)