package io.github.kmeret.bazilik.api.user.address

import io.github.kmeret.bazilik.api.api.ApiResponse
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.auth.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class AddressService {

    @Autowired
    private lateinit var addressRepository: AddressRepository
    @Autowired
    private lateinit var userService: UserService

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun findAllByUserId(userId: Long) = addressRepository.findAllByUserId(userId).map(::mapUrl)

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun save(addressDto: AddressDto): Address {
        val user = userService.getUserById(addressDto.userId)
        return addressRepository.save(Address(text = addressDto.text, user = user))
    }

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun deleteById(addressId: Long): ApiResponse {
        return try {
            addressRepository.deleteById(addressId)
            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            ApiResponse(false)
        }
    }

    fun mapUrl(address: Address) = address.apply {
        user = userService.mapUrl(user!!)
    }

}