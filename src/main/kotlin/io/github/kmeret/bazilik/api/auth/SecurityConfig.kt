package io.github.kmeret.bazilik.api.auth

import io.github.kmeret.bazilik.api.auth.api.ApiFilter
import io.github.kmeret.bazilik.api.auth.api.ApiTokenService
import io.github.kmeret.bazilik.api.auth.firebase.FirebaseFilter
import io.github.kmeret.bazilik.api.auth.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class SecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var apiTokenService: ApiTokenService
    @Autowired
    private lateinit var userService: UserService

    private val apiFilter by lazy { ApiFilter(apiTokenService) }
    private val firebaseFilter by lazy { FirebaseFilter(userService) }

    override fun configure(httpSecurity: HttpSecurity) {

        httpSecurity.cors()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().csrf().disable()

        httpSecurity
                .addFilterAfter(firebaseFilter, UsernamePasswordAuthenticationFilter::class.java)
                .addFilterAfter(apiFilter, UsernamePasswordAuthenticationFilter::class.java)

    }

}