package io.github.kmeret.bazilik.api.storage.file

import org.springframework.stereotype.Service
import java.util.*

@Service
class UIDService {

    private val alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-"
    private val r = Random()

    fun get(length: Int): String {
        var uid = ""
        var q = length
        while (q-- > 0) {
            uid += alphabet[r.nextInt(alphabet.length)]
        }
        return uid
    }

}