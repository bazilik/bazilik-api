package io.github.kmeret.bazilik.api.user.contact

import io.github.kmeret.bazilik.api.api.ApiResponse
import io.github.kmeret.bazilik.api.auth.role.Roles
import io.github.kmeret.bazilik.api.auth.user.UserRepository
import io.github.kmeret.bazilik.api.auth.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class ContactService {

    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var userService: UserService

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun importContacts(contactsDto: ContactsDto): ApiResponse {
        return try {
            val user = userRepository.findById(contactsDto.userId).get()

            contactsDto.phoneList.forEach {
                val friend = userRepository.findByPhone(it)
                if (friend != null && !user.friendList.contains(friend))
                    user.friendList.add(friend)
            }

            userRepository.save(user)

            ApiResponse(true)
        } catch (ex: IllegalArgumentException) {
            println(ex.message)
            ApiResponse(false)
        }
    }

    @Secured(value = [(Roles.ROLE_USER), (Roles.ROLE_GOD)])
    fun getFriendListByUserId(userId: Long) = userService.getFriendListByUserId(userId)

}