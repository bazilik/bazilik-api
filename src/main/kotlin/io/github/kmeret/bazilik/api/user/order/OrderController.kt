package io.github.kmeret.bazilik.api.user.order

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("order")
class OrderController {

    @Autowired
    private lateinit var orderService: OrderService

    @GetMapping("delivered")
    fun getDeliveredDishList(@RequestParam("userId") userId: Long) =
            orderService.findAllDeliveredByUserId(userId)

    @GetMapping("undelivered")
    fun getUndeliveredDishList(@RequestParam("shopId") shopId: Long) =
            orderService.findAllUndeliveredByShopId(shopId)

    @PostMapping("create")
    fun save(@RequestBody createOrderDto: CreateOrderDto) =
            orderService.save(createOrderDto)

    @PostMapping("complete")
    fun complete(@RequestParam orderId: Long) =
            orderService.complete(orderId)

}