package io.github.kmeret.bazilik.api.auth.role

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class RolesInitialLoader : ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Transactional
    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        Roles.ROLE_LIST.forEach {
            createRoleIfNotFound(it)
        }
    }

    @Transactional
    fun createRoleIfNotFound(name: String) {
        var role = roleRepository.findByName(name)
        if (role != null) return

        role = Role(name = name)
        roleRepository.save(role)
    }

}