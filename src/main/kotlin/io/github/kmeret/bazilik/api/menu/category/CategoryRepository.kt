package io.github.kmeret.bazilik.api.menu.category

import org.springframework.data.repository.CrudRepository

interface CategoryRepository : CrudRepository<Category, Long> {
    fun findAllByShopId(shopId: Long): Iterable<Category>
}